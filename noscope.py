#!/usr/bin/env python3.7
# coding=utf-8

import os
import sys
import time
import json
import argparse
import psutil
import win32ui
import win32gui
import win32process
import win32api
import win32con
import requests
import datetime

from urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter

s = requests.Session()

DEFAULT_CONFIG_FILE = "config.json"
VERSION = "0.0.8d"

ctrlCodeMap = dict((getattr(win32con, k), k) for k in ("CTRL_C_EVENT", "CTRL_BREAK_EVENT", "CTRL_CLOSE_EVENT",
                                                       "CTRL_LOGOFF_EVENT", "CTRL_SHUTDOWN_EVENT"))
retries = Retry(total=5,
                backoff_factor=0.1,
                status_forcelist=[500, 502, 503, 504])


def WindowExists(procname):
    try:
        return win32ui.FindWindow(None, procname)
    except win32ui.error:
        return False


def get_hwnds_for_pid(pid):
    def callback(hwnd, hwnds):
        _, found_pid = win32process.GetWindowThreadProcessId(hwnd)
        if found_pid == pid:
            hwnds.append(hwnd)
        return True

    hwnds = []
    win32gui.EnumWindows(callback, hwnds)
    return hwnds


def setZoomCameraState(cfg, state):
    c = cfg["active" if state else "inactive"]
    response = s.post(c["url"], data=c["data"], json=c["json"], headers=dict(cfg["headers"], **c["headers"]))


def ping(cfg, on=True):
    c = cfg["ping"]
    if not c["enabled"]:
        return
    response = s.post(c["url"].format("on" if on else "off"), data=c["data"], json=c["json"], headers=dict(cfg["headers"], **c["headers"]))


def ping_on(cfg):
    # crap solution because last_updated doesn't change on multiple turn_on events
    ping(cfg, on=False)
    ping(cfg, on=True)


def stop(cfg, sig=None):
    if args.verbose:
        print("stop called, sig: {}".format(ctrlCodeMap.get(sig, sig)))
    ping(cfg, on=False)


def teamsActive():
    active = False
    for procName in ("Teams", "Microsoft Teams"):
        main = WindowExists(procName)
        teamsCount = 0
        if main:
            _, pid = win32process.GetWindowThreadProcessId(main.GetSafeHwnd())
            for hwnd in get_hwnds_for_pid(pid):
                title = win32gui.GetWindowText(hwnd)
                if "Besprechung" in title or "Meet" in title:
                    active = True
                    break
                elif "Microsoft Teams" in title and "Benachrichtigung" not in title and "Kalender" not in title:
                    teamsCount += 1

            if (teamsCount > 1 and procName == "Teams") or teamsCount >= 2:
                active = True
                print("Active: Teams")

    return active


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="NoScope {}: Detect if Zoom camera in use, by unconventional "
                                                 "but blazing fast means".format(VERSION))
    parser.add_argument("-o", "--once", action="store_true",
                        help="Print current info once, then exit", default=False)
    parser.add_argument("-t", "--min_threads", required=False, default=15, type=int,
                        help="The thread count of aomhost to consider camera active")
    parser.add_argument("-i", "--interval", required=False, default=1.0, type=float,
                        help="How often to check for thread usage of aomhost")
    parser.add_argument("-d", "--debounce", required=False, default=5, type=int,
                        help="Avoid rapid on/off states by allowing the change "
                             "from on to off only every {debounce} seconds (can be 0)")
    parser.add_argument("-c", "--config", required=False, default=DEFAULT_CONFIG_FILE, type=str)
    parser.add_argument("--enable_ping", required=False, default=None)
    parser.add_argument("-j", "--just_ping", required=False, default=False, action="store_true")
    parser.add_argument("-p", "--progress", required=False, default=False, action="store_true")
    parser.add_argument("-v", "--verbose", required=False, default=False, action="store_true")

    args = parser.parse_args()
    verbose = args.verbose

    config_file = args.config
    if config_file == DEFAULT_CONFIG_FILE:
        config_file = os.path.join(os.path.dirname(__file__), DEFAULT_CONFIG_FILE)

    try:
        with open(config_file) as f:
            config = json.load(f)
    except:
        print("config.json missing or invalid")

    active = False
    was_active = None
    last_active = None
    last_ping = None
    if args.enable_ping in ("True", "False"):
        enable_ping = args.enable_ping == "True"
    else:
        enable_ping = config["ping"]["enabled"]

    just_ping = config["just_ping"] or args.just_ping

    win32api.SetConsoleCtrlHandler(lambda sig: stop(config, sig=sig), True)
    s.mount('http://', HTTPAdapter(max_retries=retries))

    if enable_ping:
        ping_on(config)

    try:
        while 1:
            change = False

            if not just_ping:
                # quickly find running zoom on win32
                if os.name == 'nt':
                    main = WindowExists("Zoom")
                    if main:
                        _, pid = win32process.GetWindowThreadProcessId(main.GetSafeHwnd())
                        active = False
                        try:
                            # get process thread count detail
                            p = psutil.Process(pid=pid)

                            num_threads = list(filter(lambda p: p.name() == "aomhost.exe", p.children()))[0].num_threads()
                            if num_threads >= args.min_threads:
                                active = True
                                print("Active: Zoom")
                        except (psutil.Error, IndexError):
                            active = False
                    else:
                        # check for Teams
                        active = teamsActive()

                        # check for Huddle in Chrome
                        for proc in psutil.process_iter(attrs={"name"}):
                            chrome = None
                            pn = proc.name().lower()
                            if "chrome" in pn or "brave" in pn:
                                chrome = proc.parent() or proc

                                if chrome:
                                    for hwnd in get_hwnds_for_pid(chrome.pid):
                                        if "huddle" in win32gui.GetWindowText(hwnd).lower():
                                            active = True
                                            print("Active: Slack Huddle")
                                break
                    # if active, check whether we're locked
                    if active:
                        for proc in psutil.process_iter(attrs={"name"}):
                            if proc.name() == "LogonUI.exe":
                                active = False
                                print("We're locked, setting inactive")
                                break

                # other OS
                else:
                    for proc in psutil.process_iter(["name", "num_threads"]):
                        proc_name = proc.info["name"].lower()

                        if "aomhost" in proc_name and "64" not in proc_name:
                            if proc.info["num_threads"] > args.min_threads:
                                active = True
                                break
                            else:
                                active = False
                                break

                skip = False
                t = time.time()
                if not active:
                    # debounce if last action was active
                    if last_active and was_active and last_active + args.debounce > t:
                        skip = True
                        if args.verbose:
                            print("Debouncing")
                else:
                    last_active = t

                if was_active != active and not skip:
                    change = True
                    active_str = "active" if active else "inactive"
                    print("\nZoom detection: Setting %s" % active_str)
                    setZoomCameraState(config, active)
                    was_active = active

            if args.once:
                break

            now = datetime.datetime.now()
            if enable_ping and (not last_ping or now - last_ping > datetime.timedelta(minutes=1)):
                if args.verbose:
                    print("pinging")
                ping_on(config)
                last_ping = now

            time.sleep(args.interval)

            if args.progress:
                if not change:
                    sys.stdout.write(".")
                    sys.stdout.flush()

    except KeyboardInterrupt:
        pass
